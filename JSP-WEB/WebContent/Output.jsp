<%@page import="java.util.List"%>
<%@page import="pkg1.FormClass"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>OUTPUT</title>
</head>
<body>
	<p>
		Entered ID is:<%=session.getAttribute("id")%></p>
	<p>
		Entered Name is:<%=session.getAttribute("name")%></p>
	<p>
		Entered Password is:<%=session.getAttribute("passsword")%></p>
	<p>
		Selected languages are:<%
		String[] languages = (String[]) session.getAttribute("languages");
		for (String lang : languages)
			out.print("<br>" + lang);
	%>
	</p>

	<p>
	
	<% FormClass fc=(FormClass)session.getAttribute("formclass"); %>
	Entered ID is:<%=fc.getId() %><br>
	Entered name is:<%=fc.getName() %><br>
	Entered password is:<%=fc.getPassword() %><br>
	Entered languages are:<%List<String> speaking_languages=fc.getLanguages();
	for(String lang:speaking_languages)
		out.print("<br>"+lang);%>
	
	</p>


</body>
</html>