package pkg1;

import java.util.ArrayList;
import java.util.List;

public class FormClass {
	private int id;
	private String name;
	private String password;
	private List<String> languages;

	public FormClass() {
		this.id = 0;
		this.name = "";
		this.password = "";
		this.languages = new ArrayList<>();
	}

	public FormClass(int id, String name, String password, List<String> languages) {

		this.id = id;
		this.name = name;
		this.password = password;
		this.languages = languages;
	}

	public FormClass(FormClass fc) {
		this.id = fc.id;
		this.name = fc.name;
		this.password = fc.password;
		this.languages = fc.languages;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

	@Override
	public String toString() {
		return "FormClass [id=" + id + ", name=" + name + ", password=" + password + ", languages=" + languages + "]";
	}

}
