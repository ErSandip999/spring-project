package pkg1;

public class User {
	private int id;
	private String full_name;
	private String login_name;
	private String login_password;
	
	public User() {
		this.id=0;
		this.full_name=null;
		this.login_name=null;
		this.login_password=null;
	}

	public User(int id, String full_name, String login_name, String login_password) {
		
		this.id = id;
		this.full_name = full_name;
		this.login_name = login_name;
		this.login_password = login_password;
	}
	public User(User u) {
		this.id=u.id;
		this.full_name=u.full_name;
		this.login_name=u.login_name;
		this.login_password=u.login_password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getLogin_name() {
		return login_name;
	}

	public void setLogin_name(String login_name) {
		this.login_name = login_name;
	}

	public String getLogin_password() {
		return login_password;
	}

	public void setLogin_password(String login_password) {
		this.login_password = login_password;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", full_name=" + full_name + ", login_name=" + login_name + ", login_password="
				+ login_password + "]";
	}
	


}
