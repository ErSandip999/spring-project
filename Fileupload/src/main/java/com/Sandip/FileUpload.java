package com.Sandip;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, maxFileSize = 1024 * 1024 * 10, maxRequestSize = 1024 * 1024 * 50)
public class FileUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final String UPLOAD_DIRECTORY = "C:/uploads";

		ServletFileUpload sf = new ServletFileUpload(new DiskFileItemFactory());

		// process only if its multipart content
		// if(ServletFileUpload.isMultipartContent(request))
		try {
			List<FileItem> multifiles = sf.parseRequest(request);
			for (FileItem item : multifiles) {
				// item.write(new File("D://Uploads" + item.getName()));
				String name = new File(item.getName()).getName();
				item.write(new File(UPLOAD_DIRECTORY + File.separator + name));
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
