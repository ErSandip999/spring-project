package pkg1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Products")
public class Products extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("txt2_id"));
		String name = request.getParameter("txt2_name");
		Double rate = Double.parseDouble(request.getParameter("txt2_price"));
		int quantity = Integer.parseInt(request.getParameter("txt2_quantity"));
		Double amount = (rate * quantity);
		product p = new product(id, name, rate, quantity, amount);
		HttpSession session = request.getSession();

		List<product> productlist = new ArrayList<product>();
		productlist.add(p);
		session.setAttribute("list", productlist);

		RequestDispatcher rd = request.getRequestDispatcher("Decision.jsp");
		rd.forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);
	}

}
