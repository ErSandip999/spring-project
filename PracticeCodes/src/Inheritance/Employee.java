package Inheritance;

public class Employee {
	int id;
	String name;
	String post;
	public Employee() {
		this.id=0;
		this.name="";
		this.post="";
		
	}
	public Employee(int id, String name, String post) {
		super();
		this.id = id;
		this.name = name;
		this.post = post;
	}
	public Employee(Employee em) {
		this.id=em.id;
		this.name=em.name;
		this.post=em.post;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", post=" + post + "]";
	}
	

}
