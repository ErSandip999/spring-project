package lecture5;
import java.io.*;
public class File {

	public static void main(String[] args)throws Exception {
		// TODO Auto-generated method stub
		  FileInputStream fin1=new FileInputStream("D:\\java.txt");    
		   FileInputStream fin2=new FileInputStream("D:\\myfile.txt");    
		   FileOutputStream fout=new FileOutputStream("D:\\test.txt");      
		   SequenceInputStream sis=new SequenceInputStream(fin1,fin2);    
		   int i;    
		   while((i=sis.read())!=-1)    
		   {    
		     fout.write(i);        
		   }    
		   sis.close();    
		   fout.close();      
		   fin1.close();      
		   fin2.close();       
		   System.out.println("Success..");

	}

}
