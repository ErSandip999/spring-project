package lecture5;
import java.io.*;
public class BufferedOutputStreamExample {

	public static void main(String[] args)throws Exception {
		// TODO Auto-generated method stub
		FileOutputStream fout=new FileOutputStream("D:\\java.txt");
		BufferedOutputStream bout=new BufferedOutputStream(fout);
		String s="Welcome to java classes";
		byte b[]=s.getBytes();
		bout.write(b);
		bout.flush();
		bout.close();
		fout.close();
		System.out.println("Success");

	}

}
