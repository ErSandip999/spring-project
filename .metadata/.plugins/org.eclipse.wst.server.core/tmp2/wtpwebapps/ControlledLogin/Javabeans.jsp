<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<jsp:useBean id="obj" class="pkg11.Calculator"></jsp:useBean>
<title>Bean....</title>
</head>
<body>
<%= obj.calc_sum(1, 2) %>
</body>
</html>