<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSTL-CORE</title>
</head>
<body>
<c:out value="${'Welcome .....' }"></c:out>


<c:set var="Income" scope="session" value="${4000*4}"/>  
<c:out value="${Income}"/> <br>
<c:out value="${Income}"/>

<c:remove var="Income"/>  
<p>After Remove Value is: <c:out value="${Income}"/></p> 


<c:catch var ="ex">  
   <% int x = 2/0;%>  
</c:catch>  
  
<c:if test = "${ex != null}">  
   <p>The type of exception is : ${ex} <br />  
   There is an exception: ${ex.message}</p>  
</c:if> 



<c:set var="income" scope="session" value="${4000*4}"/>  
<p>Your income is : <c:out value="${income}"/></p> 

 
<c:choose>  
    <c:when test="${income <= 1000}">  
       Income is not good.  
    </c:when>  
    <c:when test="${income > 10000}">  
        Income is very good.  
    </c:when>  
    <c:otherwise>  
       Income is undetermined...  
    </c:otherwise>  
</c:choose> <br>

<c:forEach var="j" begin="1" end="3">  
   Item <c:out value="${j}"/><p>  
</c:forEach> <br>




<c:forEach var="j" begin="1" end="3">  
   Item <c:out value="${j}"/><p>  
</c:forEach>

 
<c:url value="/index1.jsp" var="completeURL">  
 <c:param name="trackingId" value="786"/>  
 <c:param name="user" value="Nakul"/>  
</c:url>  
${completeURL}

  
</body>
</html>