package pkg2;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Servlet6 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		double n1= Double.parseDouble(request.getParameter("txt_n1"));
		double n2= Double.parseDouble(request.getParameter("txt_n2"));
		double n3=n1+n2;
		//System.out.println(n3);
		request.setAttribute("result", Double.toString(n3));
		
		
		//to transfer values from one servlet to another we use class called request dispatcher
		//RequestDispatcher rd=request.getRequestDispatcher("seventh");
		//rd.forward(request, response);
		RequestDispatcher rd=request.getRequestDispatcher("Form5.jsp");
		rd.include(request, response);
		/* we can also use send redirect function instead of request dispatcher as follows 
		 * 
		 * 
		 * res.sendRedirect("seventh?n3="+n3);
		 * 
		 * 
		 * and in the seventh servlet we can simply write request.getparameter("n3");
		 * */
		
		
		
	}
	protected void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		doProcess(request,response);
	}
	protected void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		doProcess(request,response);
	}

}
