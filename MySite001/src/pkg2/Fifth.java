package pkg2;
import java.io.PrintWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Fifth extends HttpServlet {
	public void doProcess(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException {
		res.setContentType("text/html");
		int id=Integer.parseInt(req.getParameter("id"));
		String name= req.getParameter("name");
		String address=req.getParameter("address");
		
		PrintWriter out =res.getWriter();
		out.println("Your iD is"+id);
		out.println("Your name is"+name);
		out.println("your address is"+address);
		out.close();
		
		
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doProcess(request, response);
	}

}
