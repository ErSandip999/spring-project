package pkg1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class Second extends GenericServlet {

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out=res.getWriter();
		out.println("<h3>Second Servlet</h3>");
		//receive values 
		String str1 =req.getParameter("n1");
		String str2=req.getParameter("n2");
		
		if(str1!=null && str2!=null) {
			out.println("Value1="+str1+"<br>");
			out.println("Value2="+str2+"<br>");
			int n1,n2,n3;
			n1=Integer.parseInt(str1);
			n2=Integer.parseInt(str2);
			n3=n1+n2;
			out.println("Result:"+n3);
			
		}
		out.println("<a href='index.jsp'>Home</a>");
		out.close();
		
		
		

	}

}
