<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.DriverManager"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Database Connection using JSP</title>
</head>
<body>
	<%
		String driver = "com.mysql.jdbc.Driver";

		String dbname = "test";
		String port = "3306";
		String url = "jdbc:mysql://localhost:" + port + "/" + dbname;
		String user = "root";
		String pass = "";
		String message="";
		try{
			Class.forName(driver);
			Connection conn=DriverManager.getConnection(url,user,pass);
			conn.close();
			message="Connection to database is successful";
		}catch(Exception ex){
			message="Error:"+ex.getMessage();
			
		}
	%>
<h3>Result:<%=message %></h3>
</body>
</html>