<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<jsp:useBean id="person" class="pkg11.Person" scope="session"></jsp:useBean>
<jsp:setProperty property="*" name="person"/>
<title>Displaying....</title>
</head>
<body>
Reading........<br>
<jsp:getProperty property="fullName" name="person"/><br>
</body>
</html>