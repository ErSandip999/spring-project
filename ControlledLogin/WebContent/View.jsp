<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View_Database Here</title>
</head>

<body>

	<%
		List<Integer> IDs = (List<Integer>) request.getAttribute("list_id");
		List<String> Names = (List<String>) request.getAttribute("list_name");
		List<String> Addresses = (List<String>) request.getAttribute("list_address");
	%>

	<%
		for (int i = 0; i < IDs.size(); i++)
			out.println(IDs.get(i) + "\t" + Names.get(i) + "\t" + Addresses.get(i) + " <br> ");
	%>
	<h2>Click Either Update or Delete Button if you want to update or delete..</h2>
	<p><a href="Update.jsp">Update</a></p>
	<p><a href="Delete.jsp">Delete</a></p>
</body>
</html>