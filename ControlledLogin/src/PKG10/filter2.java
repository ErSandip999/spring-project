package PKG10;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class filter2 implements Filter {

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		PrintWriter out = response.getWriter();
		String temp = request.getParameter("txt_num1");

		try {
			Integer.parseInt(temp);
			// System.out.println(temp);
			if (Integer.parseInt(temp) > 0) {

				chain.doFilter(request, response);
			} else {
				out.println("No Negative ID's has been assigned");
				RequestDispatcher rd = request.getRequestDispatcher("form10.jsp");
				rd.include(request, response);
			}

		} catch (Exception e) {
			out.println("Error:" + e.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher("form10.jsp");
			rd.include(request, response);

		}

	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {

	}

}
