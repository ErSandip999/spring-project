package PKG10;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class filter1 implements Filter {

	@Override
	public void destroy() {
		System.out.println("hi from destroy block");

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)

			throws IOException, ServletException {
		PrintWriter out = response.getWriter();
		String tmp = request.getParameter("txt_num1");
		try {
			// System.out.println("Inside try block");
			Integer.parseInt(tmp);
			chain.doFilter(request, response);

		} catch (Exception ex) {
			System.out.println("Error:" + ex.getMessage());
			out.println("Error:" + ex.getMessage());
			RequestDispatcher rd = request.getRequestDispatcher("form10.jsp");
			rd.include(request, response);

		}

	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("hi from Init block");

	}

}
