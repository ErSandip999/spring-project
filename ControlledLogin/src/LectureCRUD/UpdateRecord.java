package LectureCRUD;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UpdateRecord extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://";
		String host = "localhost";
		String port = ":3306";
		String dbname = "test";
		String username = "root";
		String password = "";
		Connection conn;
		// Connect Database Server
		// Library(*.jar)
		// LoadDrievr
		// Connect(database Server,username,passwprd)
		// Close Connection
		// CRUD
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			String full_name = request.getParameter("full_name");
			String contact_address = request.getParameter("contact_address");
			String sql = "update tbl_person set full_name=?,contact_address=?where id=?";

			Class.forName(driver);
			conn = DriverManager.getConnection(url + host + port + "/" + dbname, username, password);
			PreparedStatement pstat = conn.prepareStatement(sql);
			pstat.setString(1, full_name);
			pstat.setString(2, contact_address);
			pstat.setInt(3, id);
			pstat.executeUpdate();
			pstat.close();
			conn.close();
			response.getWriter().println("<p>Record Updated Successfully");

		} catch (Exception ex) {
			response.getWriter().println("Error:" + ex.getMessage());
			// PrintWriter out=response.getWriter();
			// out.println("Error:"+ex.getMessage());

		}
		response.getWriter().println("<p><a href='index.jsp'>BACK</a></p>");

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doProcess(request, response);
	}

}
