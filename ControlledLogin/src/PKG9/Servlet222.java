package PKG9;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet222 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		int a, b, c;
		a = Integer.parseInt(request.getParameter("n1"));
		b = Integer.parseInt(request.getParameter("n2"));
		c = Integer.parseInt(request.getParameter("n3"));

		out.println("<p>Num1: " + a + "</p>");
		out.println("<p>Num2: " + b + "</p>");
		out.println("<p>Num3: " + c + "</p>");

		out.close();

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doProcess(request, response);
	}

}
