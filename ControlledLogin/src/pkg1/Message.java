package pkg1;

public class Message {
	User user;
	boolean flag;
	String msg;
	
	public Message() {
		this.user=new User();
		this.flag=false;
		this.msg="";
	}
	public Message(User user,Boolean flag,String msg) {
		this.user=user;
		this.flag=false;
		this.msg=msg;
	}
	public Message(Message m) {
		this.user=m.user;
		this.flag=m.flag;
		this.msg=m.msg;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	

}
