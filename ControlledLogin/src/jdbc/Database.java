package jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import pkg1.Message;
import pkg1.User;
public class Database {
	public Message doLogin (User user) {
		boolean result=false;
		try {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_users","root","");
		String sql="select * from tbl_user where login_name=?and login_password=?";
		PreparedStatement pstat=conn.prepareStatement(sql);
		
		pstat.setString(1, user.getLogin_name());
		pstat.setString(2, user.getLogin_password());
		ResultSet rs=pstat.executeQuery();
		
		while(rs.next()) {
			user=new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
			result=true;
		}
		rs.close();
		pstat.close();
		conn.close();
		if(result) {
			return new Message(user,true,"Success");
		}
		else
			return new Message(user,false,"Error");

		
		}catch(Exception ex) {
			//System.out.println("Error:"+ex.getMessage());
			return new Message(user,false,"Error:");
		}
		
	
	}
public Message saveUser(User user) {
		
		boolean result=false;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/db_users","root","");
			
			String sql="Insert into tbl_user (full_name,login_name,login_password)values(?,?,?)";
			

			PreparedStatement pstat=conn.prepareStatement(sql);
			//pstat.setInt(1, user.getId()); because id is auto incremented
			pstat.setString(1, user.getFull_name());
			pstat.setString(2, user.getLogin_name());
			pstat.setString(3, user.getLogin_password());
			
			pstat.executeUpdate();
			pstat.close();
			conn.close();result=true;
			if(result) {
				
				//System.out.println(result);
				return new Message(user,true,"Success");
				
			}
			else
				
				return new Message(user,false,"Error");
			
			
		}catch(Exception ex) {
			
			return new Message(user,false,ex.getMessage());
			
			
		}
		
		
		
		
		
	}
		



}
