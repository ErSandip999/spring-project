package pkgCRUD;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class View extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://";
		String host = "localhost";
		String port = ":3306/";
		String db_name = "test";
		String username = "root";
		String password = "";
		Connection conn;
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url + host + port + db_name, username, password);
			String sql = "Select * from tbl_person";
			PreparedStatement pstat = conn.prepareStatement(sql);
			ResultSet rs = pstat.executeQuery();
			List<Integer> list1 = new ArrayList<>();
			List<String> list2 = new ArrayList<>();
			List<String> list3 = new ArrayList<>();
			while (rs.next()) {
				// response.getWriter().println(rs.getInt(1));
				// response.getWriter().println(rs.getString(2));
				// response.getWriter().println(rs.getString(3) + "<br>");

				list1.add(rs.getInt(1));
				list2.add(rs.getString(2));
				list3.add(rs.getString(3));

			}
			request.setAttribute("list_id", list1);
			request.setAttribute("list_name", list2);
			request.setAttribute("list_address", list3);
			// response.sendRedirect("View.jsp");
			RequestDispatcher rd = request.getRequestDispatcher("View.jsp");
			rd.forward(request, response);
			pstat.close();
			conn.close();

		} catch (Exception ex) {
			response.getWriter().println("Error:" + ex.getMessage());
		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doProcess(request, response);
	}

}
