package pkg1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class database {
	Connection conn;
	private void db_connect( ) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/formdata?useTimezone=true&serverTimezone=UTC","root","");
			
		} catch (ClassNotFoundException|SQLException e) {
			System.out.println("Error:"+e.getMessage());
		}
		
	}
	private void db_close() {
		try {
			conn.close();
		}catch(Exception ex) {
			System.out.println("Error:"+ex.getMessage());
		}
	}
	public boolean insertRecord(int id,String name) {
		boolean result=false;
		String str_sql="Insert into submittedData values(?,?)";
		PreparedStatement pstat=null;
		
		try {
			db_connect();
			pstat=conn.prepareStatement(str_sql);
			pstat.setInt(1,id);
			pstat.setString(2,name);
			pstat.executeUpdate();
			pstat.close();
			db_close();
			result=true;
			
		}catch(Exception ex) {
			result=false;
			System.out.println("Error:"+ex.getMessage());
		}
		return true;
	}
	
	

}
