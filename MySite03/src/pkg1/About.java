package pkg1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class About extends GenericServlet {

	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		response.setContentType("Text/html");
		PrintWriter out=response.getWriter();
		out.println("This is the about page.");
		out.println("<p><a href="+"first"+">"+"FirstServlet"+"</a></p");
		out.println("<p><a href="+"Home"+">"+"Home"+"</a></p");
		out.close();
		

	}

}
