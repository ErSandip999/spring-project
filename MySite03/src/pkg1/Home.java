package pkg1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class Home extends GenericServlet {

	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		out.print("This is the Home Page");
		out.print("<p><a href="+"first"+">"+"FirstServlet"+"</a></p>");
		out.print("<p><a href="+"about"+">"+"About"+"</a></p>");
		out.close();
		
		

	}

}
