package pkg1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class FirstServlet extends GenericServlet {

	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		out.println("<h2>Hello! world of servlet</h2>");
		out.println("<p><a href="+"Home"+">"+"Home"+"</a></p>");
		out.println("<p><a href="+"about"+">"+"About"+"</a></p>");
		out.close();
		
		
		

	}

}
